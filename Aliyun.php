<?php



class Aliyun
{
    /**
     * 定义当前版本
     * @var string
     */
    const VERSION = '1.0.1';

    /**
     * 静态配置
     * @var DataArray
     */
    private static $config;

    /**
     * 设置及获取参数
     * @param array $option
     * @return array
     */
    public static function config($option = null)
    {
        if (is_array($option)) {
            self::$config = new DataArray($option);
        }
        if (self::$config instanceof DataArray) {
            return self::$config->get();
        }
        return [];
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws Exception
     */
    public static function __callStatic($name, $arguments)
    {
        if (substr($name, 0, 2) === 'Fi') {
            $class = 'Yon\\fi\\' . substr($name, 2);
        }
        if (!empty($class) && class_exists($class)) {
            $option = array_shift($arguments)??[]; 
            $config = is_array($option) ? $option : self::$config->get();
            return new $class($config);
        }
        throw new \Exception("class {$class} not found");
    }
    
    

}
